//[SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

//[SECTION] Schema
const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User Id is Required']
	},
	totalAmount: {
		type: Number,
		required:[true, 'Total Amount is Required']
	},
	purchasedOn: {
		type: Date,
		default: new Date
	},
	products: [ 
		{
			productId: {
			type: String,
			required: [true, 'Product Id is Required']
			},
			quantity: {
			type: Number,
			required: [true, 'Quantity is required']
			}
		}
	]	
	});
	
//[SECTION] Model
	module.exports = mongoose.model ("Order", orderSchema);