//[SECTION] Dependencies and Modules
const express = require('express');
const controller = require('../controllers/orders');
const auth = require('../auth');

//[SECTION] Routing Component
const route = express.Router();

//[SECTION] Functionalities
route.post('/checkout', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let userId = payload.id;
	let isAdmin = payload.isAdmin;
	let products = req.body.products;

	let data = {
		userId: userId,
		productList: products 
	};
	if (!isAdmin) {
		controller.creatOrder(data).then(outcome => {
			res.send(outcome);
		});
	} else {
		res.send(' Admin is Not Allowed to Purchase Product')
	}
});

route.get('/all-orders', auth.verify  ,(req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let userId = payload.id
	let isAdmin = payload.isAdmin;
	if (!isAdmin) {
		controller.getAllOrders(userId).then(outcome => {
			res.send(outcome);
		});
	} else {
		res.send('No Product was Found!');
	}
});


route.get('/:orderId', auth.verify  ,(req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let userId = payload.id

	let orderId = req.params['orderId'];
	console.log("orderId:" + orderId);

	let isAdmin = payload.isAdmin;
	if (!isAdmin) {
		controller.getAnOrder(orderId).then(outcome => {
			res.send(outcome);
		});
	} else {
		res.send('No Product was Found!');
	}
});

route.get('/', auth.verify  ,(req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let userId = payload.id
	let isAdmin = payload.isAdmin;
	if (isAdmin) {
		controller.getAllOrdersAdmin().then(outcome => {
			res.send(outcome);
		});
	} else {
		res.send('No Product was Found!')
	}
});


//[SECTION] Export Route System
 module.exports = route;	