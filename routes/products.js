//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/products');
	const auth = require('../auth');

//[SECTION] Routing Component
	const route = exp.Router();

//[SECTION]-[Post] Route
	route.post('/', auth.verify ,(req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			product: req.body,
		}
		if (isAdmin) {
			controller.addProducts(data)
		.then(outcome => {
			res.send(outcome)
		});
		} else {
			res.send('User Unauthorized to Proceed!');
		};
	});
	
//[SECTION]-[Get] Route 
	route.get('/', (req, res) => {
		controller.getAllActive().then(outcome => {
			res.send(outcome);
		});
	});

	route.get('/:id', (req, res) => {
		let id = req.params.id
		controller.getProduct(id).then(result => {
			res.send(result);
		})
	});

//[SECTION]-[Put] Route
	route.put('/:productId', auth.verify ,(req, res) => {
		let params = req.params;
		let body = req.body;		
		if (!auth.decode(req.headers.authorization).isAdmin) {
			res.send('User Unauthorized');
		} else {
			controller.updateProduct(params, body).then(outcome => {
			res.send(outcome);
			});
		};
	});

	route.put('/:productId/archive', auth.verify ,(req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
			(isAdmin) ? controller.archiveProduct(params).then(result =>res.send(result))
		 : res.send('Unauthorized User');
	});

//[SECTION]-[Delete] Route 
	route.delete('/:productId', auth.verify ,(req,res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin
		let id = req.params.courseId;
		(isAdmin) ? controller.deleteProduct(id).then(outcome =>res.send(outcome))
		: res.send('Unauthorized User');
	});

//[SECTION] Export Route System
 	module.exports = route;