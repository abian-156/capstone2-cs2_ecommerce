//[SECTION] Dependencies and Modules
const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");

//[SECTION] Functionalities
module.exports.creatOrder = async (data) => {
	let userIdentity = data.userId;
	let prodId;
	let qty;
	let total = 0;
	for(let product of data.productList) {
		prodId = product.productId;
		qty = product.quantity;

	let totalAmountForAProduct = await Product.findById(prodId).then(result => parseInt(result.price * qty));
			total = total + totalAmountForAProduct;
		}
	let newOrder = new Order({
		products : data.productList,
		totalAmount : total,
		userId : userIdentity
	});
		return newOrder.save().then((ordered, err) => {
			if (ordered) {
			let orderId = ordered._id;
			let userDb = User.findById(ordered.userId).then(result => {
			result.orders.push({orderId : orderId})
			return result.save().then((saved, err) => {
				if (err) {
					return false;
				} else {
					return true;
				}
			})
		});
			return  ordered;
			} else {
				return 'An error has occured in storing your order.'
			}
		});
	};

module.exports.getAllOrders = (userId) => {
	return User.findById(userId).then(result => {
		return result;	
	});
};


module.exports.getAnOrder = (orderId) => {
	return Order.findById(orderId).then(result => {
		console.log("Result:"+ result);
		return result;	
	});
};


module.exports.getOrderAsdmin = () => {
	return User.findById().then(result => {
		return result;	
	});
};

