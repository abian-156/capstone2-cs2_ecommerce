//[SECTION] Dependencies and Modules
	const Product = require('../models/Product');

//[SECTION] funcntionality [Create]
	module.exports.addProducts = (info) => {
		let pName = info.product.name;
		let pDesc = info.product.description;
		let pCost = info.product.price;
		let newProduct = new Product({
			name: pName,
			description: pDesc,
			price: pCost
		});
			return newProduct.save().then((savedProduct, err) => {
				if (savedProduct) {
					return savedProduct; 
				} else {
					return false;
				}
			});
		};

//[SECTION] funcntionality [Retrieve]
	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		});
	};

	module.exports.getProduct = (id) => {
		return Product.findById(id).then(result => {
			return result;
		});
	};

//[SECTION] funcntionality [Update]
	module.exports.updateProduct = (product, details) => {
		let pName = details.name;	
		let pDesc = details.description;
		let pCost= details.price;
		let updatedProduct ={	
			name: pName,
			description:pDesc,
			price: pCost
		};
		let id = product.productId;
		return Product.findByIdAndUpdate(id, updatedProduct).then(
			(productUpdated, err) => {
				if (productUpdated) {
					return true;
				} else {
					return 'Failed to Update Product';
				}
			});
	};
	module.exports.archiveProduct = (product) => {
		let id = product.productId;
			let updates = {
			isActive: false
			}
			return Product.findByIdAndUpdate(id ,updates).then (
			(archived, err) => {
				if (archived) {
					return 'Product Archived';
				} else {
					return false;
				}
			});
		};

//[SECTION] funcntionality [Delete]
	module.exports.deleteProduct = (productId) => {
		return Product.findByIdAndRemove(productId).then((removedProduct, err) => {
			if (removedProduct) {
				return 'No Product Was Removed'
			} else {
				return 'Product Successfully Removed'
			};
		});
	};